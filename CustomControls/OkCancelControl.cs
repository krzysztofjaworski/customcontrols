﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace CustomControls {
    public class OkCancelControl : Control {
        static OkCancelControl() {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(OkCancelControl), new FrameworkPropertyMetadata(typeof(OkCancelControl)));
        }

        public bool IsOpen {
            get {
                return (bool)base.GetValue(OkCancelControl.IsOpenProperty);
            }
            set {
                base.SetValue(OkCancelControl.IsOpenProperty, value);
            }
        }

        private static void IsOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            if (d is OkCancelControl inputDialogBox) {
                inputDialogBox.IsOpenChanged(e);
            }
        }

        private void IsOpenChanged(DependencyPropertyChangedEventArgs e) {
            bool flag = !(bool)e.NewValue;
            if (flag) {
                this.TryCloseWindow();
            }
        }

        public bool Result {
            get {
                return (bool)base.GetValue(OkCancelControl.ResultProperty);
            }
            set {
                base.SetValue(OkCancelControl.ResultProperty, value);
            }
        }


        public Visibility CancelButtonVisibility {
            get { return (Visibility)GetValue(CancelButtonVisibilityProperty); }
            set { SetValue(CancelButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CancelButtonVisibilityProperty =
            DependencyProperty.Register("CancelButtonVisibility", typeof(Visibility), typeof(OkCancelControl), new PropertyMetadata(Visibility.Visible));



        public ICommand OkCommand {
            get { return (ICommand)GetValue(OkCommandProperty); }
            set { SetValue(OkCommandProperty, value); }
        }

        public static readonly DependencyProperty OkCommandProperty =
            DependencyProperty.Register("OkCommand", typeof(ICommand), typeof(OkCancelControl),
                new FrameworkPropertyMetadata(null));


        public ICommand OKCmd {
            get {
                return new RelayCommand(new Action<object>(this.OKCommandMethod));
            }
        }

        private void OKCommandMethod(object obj) {
            bool _canClose = true;
            if (OkCommand != null) OkCommand.Execute(_canClose);
            if (_canClose) {
                Result = true;
                TryCloseWindow();
            }
        }

        public ICommand CancelCommand {
            get {
                return new RelayCommand(new Action<object>(this.CancelCommandMethod));
            }
        }

        private void CancelCommandMethod(object obj) {
            this.Result = false;
            this.TryCloseWindow();
        }

        private void TryCloseWindow() {
            Window _window = this.TryFindParent<Window>(base.Parent as FrameworkElement);
            bool flag = _window != null;
            if (flag) {
                _window.Close();
            }
        }

        public T TryFindParent<T>(FrameworkElement element) where T : FrameworkElement {
            bool flag = element.Parent != null;
            T result;
            if (flag) {
                bool flag2 = element.Parent is T;
                if (flag2) {
                    result = (element.Parent as T);
                } else {
                    result = this.TryFindParent<T>(element.Parent as FrameworkElement);
                }
            } else {
                result = default(T);
            }
            return result;
        }

        public bool CanExecuteOKCommand {
            get {
                return (bool)base.GetValue(OkCancelControl.CanExecuteOKCommandProperty);
            }
            set {
                base.SetValue(OkCancelControl.CanExecuteOKCommandProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(OkCancelControl), new PropertyMetadata(true, new PropertyChangedCallback(OkCancelControl.IsOpenChanged)));

        public static readonly DependencyProperty ResultProperty = DependencyProperty.Register("Result", typeof(bool), typeof(OkCancelControl), new FrameworkPropertyMetadata(false) {
            BindsTwoWayByDefault = true
        });

        public static readonly DependencyProperty CanExecuteOKCommandProperty = DependencyProperty.Register("CanExecuteOKCommand", typeof(bool), typeof(OkCancelControl), new FrameworkPropertyMetadata(true));



        public string OKButtonContent {
            get { return (string)GetValue(OKButtonContentProperty); }
            set { SetValue(OKButtonContentProperty, value); }
        }

        public static readonly DependencyProperty OKButtonContentProperty =
            DependencyProperty.Register("OKButtonContent", typeof(string), typeof(OkCancelControl), new PropertyMetadata("OK"));



        public Visibility StatusBarVisibility {
            get { return (Visibility)GetValue(StatusBarVisibilityProperty); }
            set { SetValue(StatusBarVisibilityProperty, value); }
        }

        public static readonly DependencyProperty StatusBarVisibilityProperty =
            DependencyProperty.Register("StatusBarVisibility", typeof(Visibility), typeof(OkCancelControl), new PropertyMetadata(System.Windows.Visibility.Visible));

        public double StatusBarHeight {
            get { return (double)GetValue(StatusBarHeightProperty); }
            set { SetValue(StatusBarHeightProperty, value); }
        }
        
        public static readonly DependencyProperty StatusBarHeightProperty =
            DependencyProperty.Register("StatusBarHeight", typeof(double), typeof(OkCancelControl), new FrameworkPropertyMetadata(20.0));


    }
}
