﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CustomControls {
    public class RelayCommand : ICommand {
        // Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
        public RelayCommand(Action<object> execute) : this(execute, null) {
        }

        // Token: 0x06000002 RID: 2 RVA: 0x0000205C File Offset: 0x0000025C
        public RelayCommand(Action<object> execute, Predicate<object> canExecute) {
            this._execute = execute;
            this._canExecute = canExecute;
        }

        // Token: 0x14000001 RID: 1
        // (add) Token: 0x06000003 RID: 3 RVA: 0x00002084 File Offset: 0x00000284
        // (remove) Token: 0x06000004 RID: 4 RVA: 0x000020BC File Offset: 0x000002BC
        public event EventHandler CanExecuteChanged;

        // Token: 0x06000005 RID: 5 RVA: 0x000020F4 File Offset: 0x000002F4
        public bool CanExecute(object parameter) {
            return this._canExecute == null || this._canExecute(parameter);
        }

        // Token: 0x06000006 RID: 6 RVA: 0x00002120 File Offset: 0x00000320
        public void Execute(object parameter) {
            bool flag = this._execute != null;
            if (flag) {
                this._execute(parameter);
            }
        }

        // Token: 0x06000007 RID: 7 RVA: 0x00002148 File Offset: 0x00000348
        public void OnCanExecuteChanged() {
            EventHandler canExecuteChanged = this.CanExecuteChanged;
            if (canExecuteChanged != null) {
                canExecuteChanged(this, EventArgs.Empty);
            }
        }

        // Token: 0x06000008 RID: 8 RVA: 0x00002163 File Offset: 0x00000363
        public void OnCanExecuteChanged(Predicate<object> canExecute) {
            this._canExecute = canExecute;
        }

        // Token: 0x04000002 RID: 2
        private Action<object> _execute = null;

        // Token: 0x04000003 RID: 3
        private Predicate<object> _canExecute = null;
    }
}
