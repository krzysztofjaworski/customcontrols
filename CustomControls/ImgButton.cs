using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CustomControls {
    public class ImgButton : Button {

        static ImgButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImgButton), new FrameworkPropertyMetadata(typeof(Button)));
        }

        private readonly Image _image;
        private readonly AccessText _text;

        public ImgButton()
        {
            this.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            _image = CreateImageControl();
            _text = CreateTextControl();
            DockPanel dockPanel = new DockPanel();
            dockPanel.Children.Add(_image);
            dockPanel.Children.Add(_text);
            dockPanel.LastChildFill = true;
            DockPanel.SetDock(_image, Dock.Left);
            DockPanel.SetDock(_text, Dock.Right);
            this.Content = dockPanel;
        }

        private AccessText CreateTextControl()
        {
            AccessText textControl = new AccessText {
                Margin = new Thickness(4, 0, 0, 0),
                VerticalAlignment = VerticalAlignment.Center,
                TextAlignment = TextAlignment,
                TextWrapping = TextWrapping.Wrap
            };
            return textControl;
        }

        private Image CreateImageControl()
        {
            Image image = new Image {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Height = 32,
                Width = 32
            };
            return image;
        }

        public object Text {
            get { return GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public double IconWidth {
            get { return (double)GetValue(IconWidthProperty); }
            set { SetValue(IconWidthProperty, value); }
        }

        public double IconHeight {
            get { return (double)GetValue(IconHeightProperty); }
            set { SetValue(IconHeightProperty, value); }
        }

        public ImageSource ImageSource {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty IconHeightProperty =
            DependencyProperty.Register("IconHeight", typeof(double), typeof(Image), new FrameworkPropertyMetadata(20.0, new PropertyChangedCallback(IconHeightChanged)));

        private static void IconHeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ImgButton)?.IconHeightChanged(e);
        }

        private void IconHeightChanged(DependencyPropertyChangedEventArgs e)
        {
            if (_image != null && !Double.IsNaN((double)e.NewValue))
                _image.Height = (double)e.NewValue;
        }

        public static readonly DependencyProperty IconWidthProperty =
            DependencyProperty.Register("IconWidth", typeof(double), typeof(ImgButton), new FrameworkPropertyMetadata(20.0, new PropertyChangedCallback(IconWidthChanged)));

        private static void IconWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ImgButton)?.IconWidthChanged(e);
        }

        private void IconWidthChanged(DependencyPropertyChangedEventArgs e)
        {
            if (_image != null && !Double.IsNaN((double)e.NewValue))
                _image.Width = (double)e.NewValue;
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(object), typeof(ImgButton), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(TextChanged)) { BindsTwoWayByDefault = true });

        private static void TextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ImgButton)?.TextChanged(e);
        }

        private void TextChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && _text != null)
                _text.Text = e.NewValue.ToString();
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ImgButton), new FrameworkPropertyMetadata(null, ImageChanged) { BindsTwoWayByDefault = true });

        private static void ImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ImgButton)?.ImageChanged(e);
        }

        private void ImageChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && _image != null)
                _image.Source = e.NewValue as ImageSource;

            _image.Width = IconWidth;
            _image.Height = IconHeight;
        }

        public TextAlignment TextAlignment {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(ImgButton), new FrameworkPropertyMetadata(TextAlignment.Center, new PropertyChangedCallback(TextAligmnentChanged)));

        private static void TextAligmnentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ImgButton)?.TextAligmnentChanged(e);
        }

        private void TextAligmnentChanged(DependencyPropertyChangedEventArgs e)
        {
            this._text.TextAlignment = (TextAlignment)e.NewValue;
        }

        public Thickness TextMargin {
            get { return (Thickness)GetValue(TextMarginProperty); }
            set { SetValue(TextMarginProperty, value); }
        }


        public static readonly DependencyProperty TextMarginProperty =
            DependencyProperty.Register("TextMargin", typeof(Thickness), typeof(ImgButton), new FrameworkPropertyMetadata(new Thickness(4, 0, 0, 0), new PropertyChangedCallback(TextMarginChanged)));

        private static void TextMarginChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ImgButton)?.TextMarginChanged(e);
        }

        private void TextMarginChanged(DependencyPropertyChangedEventArgs e)
        {
            _text.Margin = (Thickness)e.NewValue;
        }

        public bool IsReadOnly {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }
        
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(ImgButton), new PropertyMetadata(false));
    }
}