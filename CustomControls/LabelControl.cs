﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomControls
{
    public class LabelControl : ContentControl
    {
        public string Text {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(LabelControl), new FrameworkPropertyMetadata(string.Empty));
        public Orientation Orientation {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }        
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(LabelControl), new FrameworkPropertyMetadata(Orientation.Vertical));

        static LabelControl() {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LabelControl), new FrameworkPropertyMetadata(typeof(LabelControl)));
        }

        public bool FillLastChild {
            get { return (bool)GetValue(FillLastChildProperty); }
            set { SetValue(FillLastChildProperty, value); }
        }
        
        public static readonly DependencyProperty FillLastChildProperty =
            DependencyProperty.Register("FillLastChild", typeof(bool), typeof(LabelControl), new PropertyMetadata(true));


    }
}
